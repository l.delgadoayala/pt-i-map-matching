import csv

origen1="C:\Users\luisf\Desktop\Memoria\Arcpy\Avance\GPS160526.txt"
origen2="C:\Users\luisf\Desktop\Memoria\Arcpy\Avance\PT0013160526.txt"
origen3="C:\Users\luisf\Desktop\Memoria\Arcpy\Avance\DT1804160526.txt"
destino="C:\Users\luisf\Desktop\Memoria\Arcpy\Avance\Capa_PLT.csv"


O1=open(origen1,"r")
O2=open(origen2,"r")
O3=open(origen3,"r")
Latitud=[]
Longitud=[]
Date=[]
Hora=[]
Altitude=[]
Depth=[]
Speed=[]
Heading=[]
PT=[]
DT=[]
fila=[]
myFile=open(str(destino),'w')
writer= csv.writer(myFile)

for columnas in O1:
    columnas=columnas.strip()
    columnas=columnas.split()

    try:
        if(columnas[-1]=="true"):
            var=columnas[1]
            var=var[1:]
            var=float(var)
            var2=float(columnas[2])
            var2=var2/60
            var=var+var2
            Latitud.append(var*-1)
            var=columnas[3]
            var=var[1:]
            var=float(var)
            var2=float(columnas[4])
            var2=var2/60
            var=var+var2
            Longitud.append(var*-1)
            Date.append(columnas[5])
            var=columnas[6]

            if(len(var)<8):
                Hora.append('0'+var)
            else:
                Hora.append(var)
            Altitude.append(columnas[7])
            Depth.append(columnas[9])
            Speed.append(columnas[12])
            var=columnas[14]
            var=var[0:-1]
            Heading.append(var)


    except:

        continue
cont=0

for columnas in O2:
    columnas=columnas.strip()
    columnas=columnas.split("\t")

    try:
       if(Hora[cont]==columnas[1]):
            cont+=1
            PT.append(columnas[2])

    except:
        continue

cont=0
for columnas in O3:
    columnas=columnas.strip()
    columnas=columnas.split(",")


    try:
       if(Hora[cont]==columnas[1]):
            cont+=1
            DT.append(float(columnas[2])*1000)

    except:
        continue

if len(PT)<len(DT):
    fila=[]
    fila.append(("Latitud","Longitud","Fecha","Hora","Altitude","Depth","Speed","Heading","PT (pt/cc)","DT (um/m^3)"))

    for i in range(len(PT)):

        fila.append([Latitud[i],Longitud[i],Date[i],Hora[i],Altitude[i],Depth[i],Speed[i],Heading[i],PT[i],DT[i]])
    writer.writerows(fila)


else:
    fila=[]
    fila.append(("Latitud","Longitud","Fecha","Hora","Altitude","Depth","Speed","Heading","PT (pt/cc)","DT (um/m^3)"))

    for i in range(len(DT)):

        fila.append([Latitud[i],Longitud[i],Date[i],Hora[i],Altitude[i],Speed[i],Heading[i],PT[i],DT[i]])
    writer.writerows(fila)


O1.close()
O2.close()
O3.close()


